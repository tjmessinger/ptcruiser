import urllib.request
import urllib.parse
import json
import base64


certificate_endpoint_maps = {'host': 'v1/ssl/cert/host',
                             'serial': 'v1/ssl/cert/serial',
                             'sha1': 'v1/ssl/cert/sha1',
                             'name': 'v1/ssl/cert/name',}


class Client:
    def __init__(self, username: str, api_key: str) -> None:
        self.base_url = 'https://api.riskiq.net'
        self.auth = base64.b64encode(f'{username}:{api_key}'.encode('utf-8')).decode('utf-8') # Used solution found on https://www.pythonfixing.com/2021/12/fixed-how-to-use-urllib-with.html, even more reason to use requests library.
        self.headers = {'Content-Type': 'application/json',
                        'X-RiskIQ-ISO': 'true',
                        'X-RiskIQ-Timezone': 'UTC',
                        'Authorization': f'Basic {self.auth}'}#TODO: add timezone argument to Client args
        
        
    def get(self, endpoint: str, params: dict = None) -> dict:
        """ Builds get request with given parameters and returns response
        #TODO: implement with requests library to lower complexity
        #TODO: add error handling
        Args:
            endpoint (str): endpoint to get data from
            params (dict, optional): query parameters. Defaults to None.

        Returns:
            dict: data from endpoint
        """
        
        url = f"{self.base_url}/{endpoint}"
        
        if params:
            url += '?' + urllib.parse.urlencode(params)
        
        request = urllib.request.Request(url, data=None, headers=self.headers)
        
        with urllib.request.urlopen(request) as response: 
            return json.loads(response.read().decode('utf-8'))  # Converts incoming data to a dictionary for use in other functions
    
    def account(self) -> dict:
        """ Gets account data for user.
        
        Returns:
            dict: Account data
        """
        return self.get('pt/v2/account')
        
    def passive(self, domain: str, time_start: str = None, time_end: str = None) -> dict:
        """ Returns passive DNS for specified domain. If time_start and time_end are 
            specified, returns passive DNS for specified time range.
        Args:
            domain (str): domain to get passive data from
            time_start (str, optional): start time for passive data. Defaults to None.
            time_end (str, optional): end time for passive data. Defaults to None.
            
        Returns:
            dict: Passive DNS data for domain
        """
        
        params = {'query': domain}
        
        if time_start and time_end:
            params + {'start': time_start, 'end': time_end}
            
        return self.get('pt/v2/dns/passive', {'query': domain})
        
         
    
    def certificate(self, **kwargs) -> dict:
        """ Returns certificate data based on given search criteria. 
            Supported search criteria are: host, serial, sha1, and name. 
        Keyword Args:
            host (str, optional): host to get certificate data from
            serial (str, optional): certificate serial number to get data from
            sha1 (str, optional): certificate SHA1 to get data from
            name (str, optional): certificate SAN to get data from
            
        Raises:
            ValueError: If no search criteria is specified, or if multiple search criteria are specified.

        Returns:
            dict: Certificate data
        """
        
        if len(kwargs.keys()) > 1 or len(kwargs.keys()) == 0:
            raise ValueError("Client.certificate() takes exactly 1 argument")
        query, data = kwargs.popitem()
        if query not in certificate_endpoint_maps:
            raise ValueError("Client.certificate() takes a valid endpoint")
        
        endpoint = certificate_endpoint_maps[query] 
        
        return self.get(endpoint, {query: data})
    
    def whois(self, domain: str) -> dict:
        """ Gets whois data for specified domain.

        Args:
            domain (str): Domain to get whois data from.

        Returns:
            dict: Whois data for domain
        """
        return self.get('pt/v2/whois', {'query': domain})
        