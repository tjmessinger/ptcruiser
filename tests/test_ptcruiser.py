import pytest
import ptcruiser
import vcr # found useful for testing api calls without having to keep api key in source code. https://github.com/kiwicom/pytest-recording


@pytest.fixture
def client():    
    username = 'user'
    api_key = 'api_key'

    return ptcruiser.Client(username, api_key)

@pytest.mark.vcr
def test_account(client):
    assert client.account()['username'] == 'user@gmail.com'    

@pytest.mark.vcr
def test_passive(client):
    assert len(client.passive('purple.com')['results']) > 0

@pytest.mark.vcr
def test_whois(client):
    assert len(client.whois('purple.com')['rawText']) > 0

def test_certificate(client):
    assert False # TODO: Fix this test once the API call is fixed