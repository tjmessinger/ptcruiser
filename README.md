# ptcruiser

A Python wrapper for the Passive Total API. Covers passive dns lookup, certificate lookup, and whois lookup.


## Example  
---
```
import ptcruiser
import os


username = "user@domain.com"
api_key = os.environ["PASSIVE_TOTAL_API_KEY"]

client = ptcruiser.client(username, api_key)

results = client.passive("google.com")

for result in results:
    print(results)

```

# Installation 

```
cd ptcruiser 
pip install .
```


# Reference

## ptcruiser.Client() -> ptcruiser.Client
```
import ptcruiser
import os

username = "user@domain.com"
api_key = os.environ["PASSIVE_TOTAL_API_KEY"]

client = ptcruiser.client(username, api_key)

```


## client.passive() -> Dict
Search for passive DNS results for a domain.
```
results = client.passive("google.com")

for result in results:
    print(result)
```

### Response

```
{'firstSeen': '2018-04-13 10:08:00',
 'lastSeen': '2022-08-08 21:52:56',
 'pager': None,
 'queryType': 'domain',
 'queryValue': 'purple.com',
 'results': [{'collected': '2022-08-09 04:52:56',
              'firstSeen': '2021-05-04 15:09:56',
              'lastSeen': '2022-08-08 21:52:56',
              'recordHash': '740c61264c825b6bbbc9b5a06d9021768377a779d6ca102700cfa9ce954fd1c1',
              'recordType': 'A',
              'resolve': '104.18.28.202',
              'resolveType': 'ip',
              'source': ['riskiq', 'pingly', 'emerging_threats'],
              'value': 'purple.com'},
...
```


Reference documentation: https://api.riskiq.net/api/pdns_pt/ 

---

## client.certificate() -> Dict
Search for certificate.
```
result = client.certificate(host="google.com")
print(result)
```

### Known issues 

<sub>Currently unauthorized to use this endpoint, unsure if this is an issue with using a community account or if there is an issue with the logic behind the request to this endpoint. </sub>


Reference documentation: https://api.riskiq.net/api/ssl/

---

## client.whois() -> Dict
Get whois data for specified domain.
```
result = client.whois("google.com")
print(result)
```
### Response 
```
{'admin': {},
 'billing': {},
 'contactEmail': 'abuse@gandi.net',
 'domain': 'purple.com',
 'domainStatus': 'clientTransferProhibited',
 'expiresAt': '2027-08-29T21:00:00.000-07:00',
 'lastLoadedAt': '2022-08-01T05:36:13.890-07:00',
 'name': 'N/A',
 'nameServers': ['jay.ns.cloudflare.com', 'zita.ns.cloudflare.com'],
 'organization': 'N/A',
 'rawText': 'Registrar: \n'
            '  Handle: 45097_DOMAIN_COM-VRSN\n'
            '  LDH Name: purple.com\n'
...
```
Reference documentation: https://api.riskiq.net/api/whois_pt/

## client.account() -> Dict
Gets account information for the current authenticated user.
```
print(client.account())
```
### Response
```
{'accountStatus': 'community',
 'approvedSources': 'riskiq, pingly, osint, crawl, google_cse, '
                    'emerging_threats, threat_expert, ibm, facebook',
 'country': 'United States',
 'darkMode': False,
 'datasets': {'adversaryIntel': False,
              'analystInsights': False,
              'attackSurfaceIntel': False,
              'brandIntel': False,
 ...
```
Reference documentation: https://api.riskiq.net/api/account/#!/default/get_pt_v2_account

# Testing
After installing module, install required packages and run pytest. Testing utilized pytest and vcr so no external network calls are made to the API.
```
pip install -f ./tests/requirements.txt
cd ./tests
python -m pytest
```

# Improving the API connector,

- Currently, the api is not requiring 3rd party packages such as requests, but thought should be given to replacing the urllib implementation with requests to allow for less complex maintenance and extending of get methods error handling.
- There are quite a few extra endpoints available to add functionality for and most are low hanging fruit that can use the existing get request logic. 
- The chosen endpoint for ssl certificates did not pan out correctly. Instead of using the endpoint /v1/ssl/cert/*, /pt/v2/sslcertificate/* should be investigated as a possible solution. 
- No pagenation options are available for longer requests such as the passive dns request, providing a 'limit' parameter for the client.passive() that only gives x number of passive dns responses received from the api could be useful for some use cases.
- https://api.riskiq.net/api/bulk_enrichment/ Bulk enrichment endpoint looks to be a powerful freeform tool that can be integrated in a future release.  

# Links

https://api.riskiq.net/api/pt_started.html - PassiveTotal API documentation