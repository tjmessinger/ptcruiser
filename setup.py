import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
    name="ptcruiser",
    version="0.1",
    author="TJ",
    author_email="myemailwouldgo@here.com",#TODO: add project email
    description="A Python wrapper for PassiveTotal",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://example.com",# TODO: add url
    packages=setuptools.find_namespace_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Topic :: Security",
    ],
    py_modules=["urllib", "json", "base64"], 
)